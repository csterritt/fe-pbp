import Vue from 'vue'
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

// set default config
Vue.$cookies.config('30y')

// set global cookie
// Vue.$cookies.set('theme','default');
// Vue.$cookies.set('hover-time','1s');
// $cookies.set(keyName, value[, expireTimes[, path[, domain[, secure]]]])   //return this

// get a cookie
// Vue.$cookies.get(keyName)  // return value

// Remove a cookie
// Vue.$cookies.remove(keyName [, path [, domain]])  // return this

// Check if a cookie name exists
// Vue.$cookies.isKey(keyName)  // return false or true

// Get All cookie names
// Vue.$cookies.keys()  // return a array

// config:
// $cookies.config(expireTimes[,path[, domain[, secure]]])  // default: expireTimes = 1d, path = '/', domain = '', secure = ''
