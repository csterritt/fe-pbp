import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import './plugins/vue-cookies'

Vue.config.productionTip = false

const ignoreWarnMessage =
  'The .native modifier for v-on is only valid on components but it was used on <svg>.'
// eslint-disable-next-line
Vue.config.warnHandler = function(msg, vm, trace) {
  // `trace` is the component hierarchy trace
  if (msg === ignoreWarnMessage) {
    msg = null
    vm = null
    trace = null
  }
}

new Vue({
  vuetify,
  render: (h) => h(App),
}).$mount('#app')
